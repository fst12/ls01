#include <stdio.h>
#include <string.h>	//strlen
#include <sys/socket.h> 
#include <arpa/inet.h>

int main(int argc, char **argv)
{
	 if (argc < 2) {
		printf("Usage: ./client-tcp PORT \n");
		return 1;
	 }


	int socket_desc;
	struct sockaddr_in server;


	printf("Starting\n");

	
	// Create socket
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket\n");
		return 1;
	}
	
	// Data
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons( atoi(argv[1]) );
	
	// Bind
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		printf("Bind failed\n");
		return 1;
	}
	printf("Bind done\n");

	// Listen
	listen(socket_desc , 3);

	// Accept and incoming connection
	int new_socket , c;
	struct sockaddr_in client;

	while(1) {
		printf("\n");
		printf("Waiting for incoming connections...\n");
		c = sizeof(struct sockaddr_in);
		new_socket = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
		if (new_socket<0)
		{
			printf("Accept failed\n");
			return 1;
		}

		printf("Connection accepted\n");

		// Reply to the client
		char *message = "Go away!\n";
		write(new_socket , message , strlen(message));

		printf("Sended message\n");
	}
	return 0;

}



