#include <stdio.h>
#include <string.h>	//strlen
#include <sys/socket.h> 
#include <arpa/inet.h>

#include <stdlib.h>
#include <pthread.h>
#include <errno.h>

#include <unistd.h>

#define test_errno(msg) do{if (errno) {perror(msg); exit(EXIT_FAILURE);}} while(0)

#define MESSAGES_LEN 100

char nr_wersu;
char* wers;

int start_port;
int end_port;

char* downloaded_messages[MESSAGES_LEN] = {0};

void* runServerOnPort(void* _arg)
{
	int port = (int)_arg;
	int socket_desc;
	struct sockaddr_in server;

	printf("[%d] Starting\n", port);

	// Create socket
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		printf("[%d] Could not create socket\n", port);
		return 1;
	}
	
	// Data
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons( port );
	
	// Bind
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		printf("[%d] Bind failed\n", port);
		return 1;
	}
	printf("[%d] Bind done\n", port);

	// Listen
	listen(socket_desc , 3);

	// Accept and incoming connection
	int new_socket , c;
	struct sockaddr_in client;

	char client_msg[1];
	char send_message_id[1];
	send_message_id[0] = nr_wersu;
	int r;

	while(1) {
		printf("[%d] Waiting for incoming connections...\n", port);
		c = sizeof(struct sockaddr_in);
		new_socket = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
		if (new_socket<0)
		{
			printf("[%d] Accept failed\n", port);
			return 1;
		}

		printf("[%d] Connection accepted\n", port);

		while(1) {
			printf("[%d] Waiting for message\n", port);
			r = recv(new_socket, client_msg , 1 , 0);
			if( r < 0)
			{
				printf("[%d] Failed receiving \n", port);
				break;
			}
			else if( r == 0){
				printf("[%d] Finished receiving \n", port);
				break;
			}
			else {
				printf("[%d] Received: %c\n", port, client_msg[0]);
			}

			if(client_msg[0] == 'A'){
				write(new_socket, send_message_id, 1);
				printf("[%d] Sended message id\n", port);
			}
			if(client_msg[0] == 'B'){
				write(new_socket, wers, strlen(wers)+1); // +1 to also send \0
				printf("[%d] Sended message\n", port);
			}
		}
	}
}


void* runClient(void *_arg) {
	printf("[Client] Socket created\n");

	printf("[Client] Starting\n");

	char* addr = "127.0.0.1";
	int port = start_port;

	// int sockfd = socket(domain, type, protocol)
	int socket_desc = socket(AF_INET , SOCK_STREAM , 0);

	if (socket_desc == -1){
		printf("[Client] Failure\n");
		return 1;
	}
	printf("[Client] Socket created\n");
	printf("[Client] Conecting to %s:%d\n", addr, port);

	struct sockaddr_in server;
	server.sin_addr.s_addr = inet_addr(addr); // adres
	server.sin_family = AF_INET;
	server.sin_port = htons( port );

	printf("[Client] Waiting for response\n");

	if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		printf("[Client] Connect error\n");
		return 1;
	}

	printf("[Client] Connected\n");

	// Ask for id
	char *message_1 = "A";
	if( send(socket_desc , message_1 , 1 , 0) < 0)
	{
		printf("[Client] Send failed");
		return 1;
	}
	printf("[Client] Checking remote message id\n");

	// Receive reply
	char server_reply_1[1];
	if( recv(socket_desc, server_reply_1 , 1 , 0) < 0)
	{
		printf("[Client] Recv failed");
		return 1;
	}
	int message_id = server_reply_1[0];
	printf("[Client] Id of message: %d\n", message_id);

	if (downloaded_messages[message_id] == 0) {
		// Get message
		char *message_2 = "B";
		if( send(socket_desc , message_2 , 1 , 0) < 0)
		{
			printf("[Client] Send failed");
			return 1;
		}
		printf("[Client] Downloaded remote message\n");

		// Receive reply
		char server_reply[2000];
		if( recv(socket_desc, server_reply , 2000 , 0) < 0)
		{
			printf("[Client] Recv failed");
			return 1;
		}
		printf("[Client] Reply received: %s\n", server_reply);

		downloaded_messages[message_id] = (char*)malloc(strlen(server_reply) * sizeof(char));
		strcpy(downloaded_messages[message_id], server_reply);
	}

	return 0;
}


void* runStatus(void *_arg) {
	while(1) {
		printf("\n--- Downloaded messages ---\n");
		for(int i=0; i<MESSAGES_LEN; i++) {
			if (downloaded_messages[i] != 0) {
				printf("%d: %s\n", i, downloaded_messages[i]);
			}
		}
		printf("---------------------------\n\n");
		sleep(5);
	}
}

int main(int argc, char **argv)
{
	pthread_t id[100];
	int n = 0;

	if (argc < 3) {
		printf("Usage: ./server-tcp NR_WERSU WERS\n");
		return 1;
	}

	nr_wersu = atoi(argv[1]);
	wers = argv[2];

	start_port = 7510;
	end_port = 7512;

	for(int port=start_port; port<=end_port; port++) {
		errno = pthread_create(&id[n], NULL, runServerOnPort, (void*)port);
		test_errno("Failed pthread_create runServerOnPort");
		n++;
	}

	sleep(1);

	errno = pthread_create(&id[n], NULL, runClient, NULL);
	test_errno("Failed pthread_create runClient");
	n++;

	sleep(1);

	errno = pthread_create(&id[n], NULL, runStatus, NULL);
	test_errno("Failed pthread_create runClient");
	n++;

	for (int i=0; i < n; i++) {
		errno = pthread_join(id[i], NULL);
		test_errno("Failed pthread_join");
	}

	return 0;
}