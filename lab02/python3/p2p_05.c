/*

Bibliografia:

Sockets Tutorial, Robert Ingalls
http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html
dostęp: 2019-01-13

POSIX Threads Programming,  Barney, Lawrence Livermore National Laboratory
https://computing.llnl.gov/tutorials/pthreads/
dostęp: 2019-01-13

*/

#include <stdio.h>
#include <string.h>	//strlen
#include <sys/socket.h> 
#include <arpa/inet.h>

#include <stdlib.h>
#include <pthread.h>
#include <errno.h>

#include <unistd.h>

// Sprawdzanie błędu pthreads
#define test_errno(msg) do{if (errno) {perror(msg); exit(EXIT_FAILURE);}} while(0)

#define MESSAGES_LEN 250

// Ustawienia
char nr_wersu;
char* wers;
int start_port;
int end_port;


char* downloaded_messages[MESSAGES_LEN] = {0};
char* sources_messages[MESSAGES_LEN] = {0};


void* runServerOnPort(void* _arg)
{
	// Port serwera
	int port = (int)_arg;
	int socket_desc;
	struct sockaddr_in server;

	// Tworzenie socketa
	printf("[%d] Starting\n", port);
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		printf("[%d] Could not create socket\n", port);
		return 1;
	}
	
	
	// Nie ustawiam timeoutu bo z praktyki lepiej działa bez
	// wymagało by więcej czasu do sprawdzenia
	
	/*
	// Ustawienie timeoutu na 1s
	struct timeval timeout;      
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

	// Ustawienie timeoutu 
    if (setsockopt (socket_desc, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)
        error("setsockopt failed\n");

    if (setsockopt (socket_desc, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)
        error("setsockopt failed\n");
	*/
	
	// Dane socketa
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons( port );
	
	// Bind
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		printf("[%d] Bind failed\n", port);
		return 1;
	}
	printf("[%d] Bind done\n", port);

	// Nasłuchiwanie
	listen(socket_desc , 3);

	// Akceptowanie połączenia
	int new_socket , c;
	struct sockaddr_in client;

	char client_msg[1];
	char send_message_id[1];
	send_message_id[0] = nr_wersu;
	int r;
	
	// W pętli oczekuj na połączenia
	while(1) {
		printf("[%d] Waiting for incoming connections...\n", port);
		c = sizeof(struct sockaddr_in);
		new_socket = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
		if (new_socket<0)
		{
			// Nie powodzenie
			printf("[%d] Accept failed\n", port);
			return 1;
		}

		// Powiodło się połączenie
		printf("[%d] Connection accepted\n", port);

		// W pętli odpowiadaj na requesty
		while(1) {
			// Oczekiwanie na request
			printf("[%d] Waiting for message\n", port);
			r = recv(new_socket, client_msg , 1 , 0);
			if( r < 0)
			{
				// Nie powodzenie
				printf("[%d] Failed receiving \n", port);
				break;
			}
			else if( r == 0){
				// Koniec pobierania gdy nie ma więcej już 
				printf("[%d] Finished receiving \n", port);
				break;
			}
			else {
				// Pobrano request
				printf("[%d] Received: %c\n", port, client_msg[0]);
			}

			if(client_msg[0] == 'A'){
				// Odpowiedz numerem wiersza
				write(new_socket, send_message_id, 1);
				printf("[%d] Sended message id\n", port);
			}
			if(client_msg[0] == 'B'){
				// Odpowiedz wierszem
				write(new_socket, wers, strlen(wers)+1); // +1 to also send \0
				printf("[%d] Sended message\n", port);
			}
		}
	}
}


void* tryClient(int ip_i, int port) {
	// printf("[C %d] Starting\n", ip_i);
	
	char* addr[20];
	// Generowanie adresu kontaktowego
	sprintf(addr, "192.168.102.%d", ip_i);
	//sprintf(addr, "192.168.102.78");
	
	// Tworzenie socketu
	// int sockfd = socket(domain, type, protocol)
	int socket_desc = socket(AF_INET , SOCK_STREAM , 0);

	if (socket_desc == -1){
		// printf("[C %d] Failure\n", ip_i);
		return 1;
	}
	// printf("[C %d] Socket created\n", ip_i);
	//printf("[C %d] Conecting to %s:%d\n", ip_i, addr, port);

	// Tak jak w serwerze, lepiej działa bez, żeby dobrze ustawić
	// pasowało by więcje czasu poświęcić
	/*
	// Ustawienie timeoutu na 1s
	struct timeval timeout;      
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

	// Ustawienie timeoutu 
    if (setsockopt (socket_desc, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)
        error("setsockopt failed\n");

    if (setsockopt (socket_desc, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout,
                sizeof(timeout)) < 0)
        error("setsockopt failed\n");
	*/
	
	// Podłaczanie do serwera
	struct sockaddr_in server;
	server.sin_addr.s_addr = inet_addr(addr); // adres
	server.sin_family = AF_INET;
	server.sin_port = htons( port );

	// Oczekiwanie na odpowiedź
	// printf("[C %d] Waiting for response\n", ip_i);
	if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		// printf("[C %d] Connect error\n", ip_i);
		return 1;
	}

	printf("[C %d] Connected to %s:%d\n", ip_i, addr, port);

	// Zapytanie o id
	char *message_1 = "A";
	if( send(socket_desc , message_1 , 1 , 0) < 0)
	{
		printf("[C %d] Send failed", ip_i);
		return 1;
	}
	printf("[C %d] Checking remote message id\n", ip_i);

	// Odbieranie id
	char server_reply_1[1];
	if( recv(socket_desc, server_reply_1 , 1 , 0) < 0)
	{
		printf("[C %d] Recv failed\n", ip_i);
		return 1;
	}
	int message_id = server_reply_1[0];
	printf("[C %d] Id of message: %d\n", ip_i, message_id);

	// Jeśli id jest nie ściągnięty to ściąga
	if (downloaded_messages[message_id] == 0) {
		// Zapytanie o treść
		char *message_2 = "B";
		if( send(socket_desc , message_2 , 1 , 0) < 0)
		{
			printf("[C %d] Send failed\n", ip_i);
			return 1;
		}
		printf("[C %d] Downloaded remote message\n", ip_i);

		// Odebranie treści
		char server_reply[2000];
		if( recv(socket_desc, server_reply , 2000 , 0) < 0)
		{
			printf("[C %d] Recv failed\n", ip_i);
			return 1;
		}
		printf("[C %d] Reply received: %s\n", ip_i, server_reply);

		// Zapisywanie wiadomości
		downloaded_messages[message_id] = (char*)malloc(strlen(server_reply) * sizeof(char));
		strcpy(downloaded_messages[message_id], server_reply);
		
		// Zapisywanie źródła - odkrytego
		char* source[50];
		sprintf(source, "%s:%d", addr, port);
		sources_messages[message_id] = (char*)malloc(strlen(source) * sizeof(char));
		strcpy(sources_messages[message_id], source);
		
		// Pobrane od tego ip
	}
	
	shutdown(socket_desc, 2);
	
	return 0;
}

void* runClient(void *_arg) {
	int ip_i = (int)_arg;
	while (1) {
		int port;
		for(port=start_port; port<=end_port; port++) {
			if (tryClient(ip_i, port) == 0) {
				return 0;
			}
		}
		sleep(10);
	}
}


void* runStatus(void *_arg) {
	while(1) {
		// Wyświetlanie wszystkich pobranych
		printf("\nPobrane wiadomości: \n");
		int i;
		for(i=0; i<MESSAGES_LEN; i++) {
			if (downloaded_messages[i] != 0) {
				printf("[%s] %d: %s\n", sources_messages[i], i, downloaded_messages[i]);
			}
		}
		printf("\n");
		// Co 5 sekund
		sleep(5);
	}
}

int main(int argc, char **argv)
{
	// Lista wątków
	pthread_t id[1000];
	// Ilość wątków
	int n = 0;

	// Sprawdzenie argumentów wejściowych
	if (argc < 4+1) {
		printf("Usage: ./server-tcp START_PORT END_PORT NR_WERSU WERS\n");
		return 1;
	}

	// Przypisanie ustawień
	start_port = atoi(argv[1]);
	end_port = atoi(argv[2]);
	nr_wersu = atoi(argv[3]);
	wers = argv[4];

	// Tworzenie wątków - serwerów
	int port;
	for(port=start_port; port<=end_port; port++) {
		errno = pthread_create(&id[n], NULL, runServerOnPort, (void*)port);
		test_errno("Failed pthread_create runServerOnPort");
		n++;
	}

	// Wstępcze oczekiwanie przed wypisaniem statystyk
	sleep(1);

	// Tworzenie wątku statystyk
	errno = pthread_create(&id[n], NULL, runStatus, NULL);
	test_errno("Failed pthread_create runClient");
	n++;

	// Oczekiwanie - włączanie klienta po serwerach
	sleep(1);

	// Tworzenie wątków - klientów
	int ip_i;
	for(ip_i=1; ip_i<255; ip_i++) {
		//printf("Starting client %d\n", ip_i);
		errno = pthread_create(&id[n], NULL, runClient, (void*)ip_i);
		test_errno("Failed pthread_create runClient");
		n++;
		//usleep(100);
	}

	// Złączanie wątków
	int i;
	for (i=0; i < n; i++) {
		errno = pthread_join(id[i], NULL);
		test_errno("Failed pthread_join");
	}

	return 0;
}
