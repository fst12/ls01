#!/usr/bin/env python3

import socket
import struct


TCP_IP = '0.0.0.0'
TCP_PORT = 7500
BUFFER_SIZE = 1
MESSAGE_ID = 4
MESSAGE = b"Widze i opisuje, bo tesknie po tobie"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

while True:
    print('Waiting for connection')
    conn, addr = s.accept()
    try:
        print('Connection address:', addr)
        while True:
            data = conn.recv(BUFFER_SIZE)
            if not data:
                break
            print("Received data:", data)
            if data == b'A':
                print("Sending message id:", MESSAGE_ID)
                conn.send(struct.pack('b', MESSAGE_ID))
            elif data == b'B':
                print("Sending message:", MESSAGE)
                conn.send(MESSAGE + b'\0')
            else:
                print("Unknown command")
    finally:
        print('Closing connection')
        conn.close()
