#!/usr/bin/env python3

import socket
import struct
from time import sleep


TCP_IP = '127.0.0.1'
TCP_PORT = 7500
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.send(b'A')
data = s.recv(BUFFER_SIZE)

message_id, = struct.unpack('b', data)
print("Received message id:", message_id)

if message_id == 4:
    s.send(b'B')
    message = s.recv(BUFFER_SIZE)[:-1].decode('ascii')
    print(message)

sleep(10)

s.close()
