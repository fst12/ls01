#!/usr/bin/env python
# -*- coding: utf-8 -*-

import threading
import logging
import socket
import select
import struct
from time import sleep

logging.basicConfig(
    level=logging.INFO,
    format='[%(levelname)s] (%(threadName)-22s) %(message)s',
)

TCP_IP = '0.0.0.0'
BUFFER_SIZE = 1
TIMEOUT = 10


class ServerThread(threading.Thread):
    def __init__(self, port, message_id, message_content):
        super(ServerThread, self).__init__(name="Server {}".format(port))
        self.port = port
        self.message_id = message_id
        self.message_content = message_content

    def run(self):
        while True:
            try:
                logging.info("Started server")

                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                # s.setblocking(False)
                s.bind((TCP_IP, self.port))
                try:
                    s.listen(1)
                    logging.info('Waiting for connection')

                    # r, w, x = select.select([s], [], [])
                    # logging.info('Connecting')

                    while True:
                        conn, addr = s.accept()
                        try:
                            logging.info('{} Connected'.format(addr))
                            while True:
                                data = conn.recv(BUFFER_SIZE)
                                if not data:
                                    break
                                logging.info("{} Received data: {}".format(addr, data))
                                if data == b'A':
                                    logging.info("{} Sending message id: {}".format(addr, self.message_id))
                                    conn.send(struct.pack('b', self.message_id))
                                elif data == b'B':
                                    logging.info("{} Sending message:".format(addr, self.message_content))
                                    conn.send(self.message_content + b'\0')
                                else:
                                    logging.info("{} Unknown command: {}".format(addr, data))
                        finally:
                            logging.info('{} Closing connection'.format(addr))
                            conn.close()
                finally:
                    s.close()
                    logging.info("Closed server")
            except KeyboardInterrupt:
                raise
            except:
                pass


class ClientThread(threading.Thread):
    def __init__(self, addr, port_first, port_count, messages):
        super(ClientThread, self).__init__(name="Client {}".format(addr))
        self.addr = addr
        self.port_first = port_first
        self.port_count = port_count
        self.messages = messages

    def run(self):
        for port in range(self.port_first, self.port_first+self.port_count):
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((TCP_IP, port))
            s.send(b'A')
            data = s.recv(BUFFER_SIZE)

            message_id, = struct.unpack('b', data)
            logging.info("Message id: {}".format(message_id))

            if message_id not in self.messages:
                s.send(b'B')
                message_content = s.recv(BUFFER_SIZE)[:-1].decode('ascii')
                logging.info("Message content: {}".format(message_content))
                self.messages[message_id] = message_content
            s.close()


def run(port_first, port_count, message_id, message_content):
    messages = {}
    server_threads = []
    for port in range(port_first, port_first+port_count):
        t = ServerThread(
            port=port,
            message_id=message_id,
            message_content=message_content,
        )
        server_threads.append(t)
        t.daemon = True
        t.start()
    client_thread = ClientThread(
        addr="127.0.0.1",
        port_first=port_first,
        port_count=port_count,
        messages=messages,
    )
    client_thread.daemon = True
    client_thread.start()
    while True:
        sleep(1)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--port_first', type=int, help="Wartość pierwszego portu", default=7500)
    parser.add_argument('-c', '--port_count', type=int, help="Ilość portów", default=10)
    parser.add_argument('message_id', type=int, help="Id wiadomości")
    parser.add_argument('message_content', help="Treść wiadomości")
    args = parser.parse_args()

    run(
        port_first=args.port_first,
        port_count=args.port_count,
        message_id=args.message_id,
        message_content=args.message_content,
    )
