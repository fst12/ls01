#include <stdio.h>
#include <string.h>	//strlen
#include <sys/socket.h> 
#include <arpa/inet.h>

int main(int argc, char **argv)
{
	if (argc < 3) {
		printf("Usage: ./client-tcp IP PORT \n");
		return 1;
	}

	printf("Client starting\n");

	// int sockfd = socket(domain, type, protocol)
	int socket_desc = socket(AF_INET , SOCK_STREAM , 0);

	if (socket_desc == -1){
		printf("Failure\n");
		return 1;
	}
	printf("Socket created\n");


	struct sockaddr_in server;
	server.sin_addr.s_addr = inet_addr(argv[1]); // adres
	server.sin_family = AF_INET;
	server.sin_port = htons( atoi(argv[2]) );

	if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		printf("connect error\n");
		return 1;
	}

	printf("Connected\n");

	// Send data
	char *message = "B";
	if( send(socket_desc , message , strlen(message) , 0) < 0)
	{
		printf("Send failed");
		return 1;
	}
	printf("Data Send\n");

	char server_reply[2000];

	//Receive a reply from the server
	if( recv(socket_desc, server_reply , 2000 , 0) < 0)
	{
		printf("recv failed");
		return 1;
	}
	printf("Reply received, and reads as follows:\n");
	printf(server_reply);
	printf("\n");

	return 0;
}



