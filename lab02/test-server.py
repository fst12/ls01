import socket
import struct

TCP_IP = '0.0.0.0'
TCP_PORT = 7500
BUFFER_SIZE = 1
MESSAGE_ID = 4
MESSAGE = "Widze i opisuje, bo tesknie po tobie"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

while True:
	try:
		conn, addr = s.accept()

		print 'Connection address:', addr

		while True:
			data = conn.recv(BUFFER_SIZE)
			if not data: break
			print "received data:", data
			if data=='A':
				conn.send(struct.pack('b', MESSAGE_ID))
			if data=='B':
				conn.send(MESSAGE+'\0')
	finally:
		print('close')
		conn.close()

